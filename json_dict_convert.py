#!/usr/bin/python3
from json import *
if __name__=="__main__":   
   d={}
   d['a'] =1
   d['b']=2
   d[3]='c'
   d[4]=['k','k1']  
   #dict to json
   k=JSONEncoder().encode(d)
   print(type(k))
   print(k)
   #json to dict
   json_str='{"a":1,"b":2,"3":"c","4":["k","k1"]}'
   d=JSONDecoder().decode(json_str)
   print(type(d))
   print(d)